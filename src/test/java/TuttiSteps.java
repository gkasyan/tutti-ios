import com.thoughtworks.gauge.Step;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TuttiSteps {

    private final IOSDriver driver;

    public TuttiSteps() {
        this.driver = DriverFactory.getDriver();
    }

    @Step("Skip intro")
    public void skipIntro() {
        driver.findElementByName("Skip").click();
    }

    @Step("Go to Settings")
    public void goToSettings() {
        driver.findElementByName("Settings").click();
    }

    @Step("On Settings page select <Account> tab")
    public void onSettingsPageSelect1Tab(String tabName) {
        driver.findElementByName(tabName).click();
    }

    @Step("On Account tab click <LOGIN> button")
    public void onAccountTabClick1Button(String name) {
        driver.findElementByName(name).click();
    }

    @Step("On LOGIN page set e-mail field to <gennadiy.kasyan@gmail.com>")
    public void setEmail(String arg0) {
        driver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIATextField[1]").sendKeys(arg0);
    }

    @Step("On LOGIN page set password field to <tutti.ch>")
    public void setPassword(String arg0) {
        driver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIASecureTextField[1]").sendKeys(arg0);
    }

    @Step("On LOGIN page tap on the checkmark icon")
    public void tapOnCheckmarkIcon() {
        driver.findElementByName("checkmark icon").click();
    }

    @Step("On Account tab there is a message <You are logged in as ><USER>")
    public void methodName2(String youAreLoggedInAs, String user) {
        String message = driver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIAStaticText[1]").getAttribute("value");
        Assert.assertEquals("Expected:" + youAreLoggedInAs + user + ", found:" + message, youAreLoggedInAs + user, message);
    }

    @Step("Click on Add button")
    public void clickOnAddButton() {
        driver.findElementByName("Add").click();
    }

    @Step("Set new item description to <My item description 1>")
    public void setItemDescription(String desc) {
        driver.findElement(By.xpath("//UIATextField[1]")).sendKeys(desc);
    }

    @Step("Add an image to the item")
    public void addAnImageToTheItem() {
        driver.findElement(By.name("add_image")).click();
        driver.findElement(By.name("Library")).click();

        if (WDUtils.isPresentWithWait(By.name("Don’t Allow")))
            driver.findElement(By.name("OK")).click();

        driver.findElement(By.name("Camera Roll")).click();
        driver.findElement(By.xpath("//UIACollectionCell[1]")).click();
    }

    @Step("Confirm add item")
    public void confirmAddItem() {
        driver.findElement(By.name("checkmark icon")).click();

        if (WDUtils.isPresentWithWait(By.name("Don’t Allow")))
            driver.findElement(By.name("Allow")).click();
    }

    @Step("Check on Your Values page there is item <My item description 1>")
    public void checkTheItemPresent(String item) {
        Assert.assertTrue("Not at Your Values page!",
                driver.findElement(By.name("Your Values")).getAttribute("value").equals("1"));
        Assert.assertTrue("Item '" + item + "' not found on Your Values page",
                WDUtils.isPresentWithWait(
                        By.xpath("//UIATableCell[@name='" + item + "']")));
    }

    @Step("Swipe right on item <My item description 1>")
    public void swipeRightOnItem(String item) {
        WebElement el0 = driver.findElement(By.xpath("//UIATableCell[@name='" + item + "']"));
        WebElement el1 = driver.findElement(By.name("CHF  ?"));
        TouchAction action = new TouchAction(driver);
        action.press(el0, 1, 1).moveTo(el1).release().perform();
    }

    @Step("Click on <delete icon>")
    public void clickOn1(String name) {
        driver.findElementByName(name).click();
    }

    @Step("Click on <Yes> button")
    public void clickOn1Button(String name) {
        driver.findElementByName(name).click();
    }

    @Step("Check on Your Values page there is no item <My item description 1>")
    public void checkTheItemNotPresent(String item) throws InterruptedException {
        Assert.assertTrue("Not at Your Values page!",
                driver.findElement(By.name("Your Values")).getAttribute("value").equals("1"));
        //the item may be still present for a few moments
        Thread.sleep(1000);
        Assert.assertFalse("Item '" + item + "' not expected on Your Values page",
                WDUtils.isPresent(By.xpath("//UIATableCell[@name='" + item + "']")));
    }

    @Step("Check there is a message <Please describe your item, so we can estimate its value for you>")
    public void checkThereIsAMessage(String message) {
        Assert.assertTrue("Expected message '" + message + "' not found",
                WDUtils.isPresentWithWait(
                        By.xpath("//UIAStaticText[@label='" + message + "']")));
    }

    @Step("Check on Add Item page there is no Add Image button")
    public void noAddImageButton() {
        Assert.assertFalse("Add Image button not expected on Add Item page",
                WDUtils.isPresent(By.name("add_image")));
    }

    @Step("Reset Add item from")
    public void resetAdditemForm() {
        if (driver.findElement(By.name("cancel icon")).isEnabled())
            driver.findElement(By.name("cancel icon")).click();
    }
}