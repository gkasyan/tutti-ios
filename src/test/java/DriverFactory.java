/**
 * Created by geyzer on 25/11/15.
 */
import com.thoughtworks.gauge.AfterSpec;
import com.thoughtworks.gauge.AfterSuite;
import com.thoughtworks.gauge.BeforeSpec;
import com.thoughtworks.gauge.BeforeSuite;
import io.appium.java_client.ios.IOSDriver;
import org.hamcrest.core.IsNull;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    public static IOSDriver getDriver() {
        return driver;
    }

    private static IOSDriver driver = null;

    //@BeforeSuite
    @BeforeSpec
    public void Setup() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.0");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "9.1");
        capabilities.setCapability("deviceName", "iPhone 5s");
        capabilities.setCapability("app", System.getenv("appPath"));
        driver = new IOSDriver(new URL("http://" + System.getenv("appiumServer").trim() + "/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        new TuttiSteps().skipIntro();
    }

    //@AfterSuite
    @AfterSpec
    public void TearDown() {
        if (driver != null)
            driver.quit();
    }
}
