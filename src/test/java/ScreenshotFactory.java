import com.thoughtworks.gauge.screenshot.ICustomScreenshotGrabber;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

/**
 * Created by geyzer on 25/11/15.
 */
public class ScreenshotFactory implements ICustomScreenshotGrabber {
    public byte[] takeScreenshot() {
        AppiumDriver driver = DriverFactory.getDriver();
        return ( driver).getScreenshotAs(OutputType.BYTES);
    }
}
