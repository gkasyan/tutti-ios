import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by geyzer on 29/11/15.
 */
public class WDUtils {
    public static final int SHORT_TIMEOUT = 2;

    public WDUtils() {
        //this.driver = DriverFactory.getDriver();
    }

    public static boolean isPresentWithWait(By by) {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), SHORT_TIMEOUT);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (TimeoutException ignore) {
            return false;
        }
        return true;
    }


    public static boolean isPresent(By by) {
        return !DriverFactory.getDriver().findElements(by).isEmpty();
    }

}
