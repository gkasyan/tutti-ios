# Test engineer challenge IOS test #

### About ###

The project uses Appium and Thoughtworks Gauge framework. Gauge is similar to Cucumber.
I use it because I used its predecessor, Twist, and decided to take this occasion and have look at the new version.
It has some advantages over Cucumber or JBehave.
Also I want to show approach in testing using business language. With good Domain Specific Language developer testers/SME may design and un test cases with little technical knowledge.
The other web application test automation project uses different, closer to unit testing style.
Cannot say at this stage, which is the right one for tutti.ch.
My goal is to show what is there in the test automation world.

### How do I get set up? ###

Install Gauge according to 
http://getgauge.io/documentation/user/current/getting_started/download_and_install.html
Add Java runner: gauge install java
Add html report: gauge --install html-report

Install Gauge plugin into IDEA or Eclipse
http://getgauge.io/documentation/user/current/plugins/list.html

Install Appium application.
Set Appium IP to 127.0.0.1

### How to run the tests? ###

Set in "env/default/default.properties" path to the application
for example:
appPath = /Users/geyzer/Library/Developer/Xcode/DerivedData/MIPA-gdmbeyvkjpcjzbgzdhzqwyysbrcb/Build/Products/Debug-iphonesimulator/MIPA.app

Appium server has to be started before running the tests
Documentation on execution from Gauge http://getgauge.io/documentation/user/current/execution/index.html

There are several options to run the tests"
- run "mvn test" in the project folder 
- use IDE Gauge plugin
- run "gauge specs/" in the project folder
  env/default/java.properties contains path to the java lib dependencies, "target/lib". It is created after "mvn package" command. I already added this folder to the repository for your convenience.

There is a full test run report in "reports/html-report"