Item insertion process
=====================
Created by geyzer on 25/11/15

Skip intro
* Click on Add button
* Reset Add item from
     
Insert an item with an image
----------------

* Add an image to the item
* Set new item description to "My item desc 1"
* Confirm add item
* Check on Your Values page there is item "My item desc 1"


Insert an item with an image and no description
----------------

* Add an image to the item
* Confirm add item
* Check there is a message "Please describe your item, so we can estimate its value for you"

Insert an item without an image
----------------

* Set new item description to "My item desc 2"
* Confirm add item
* Check on Your Values page there is item "My item desc 2"

Insert an item with 4 images
----------------

Check there is no more add image button after 4 images

* Add an image to the item
* Add an image to the item
* Add an image to the item
* Add an image to the item
* Check on Add Item page there is no Add Image button
* Set new item description to "My item desc 3"
* Confirm add item
* Check on Your Values page there is item "My item desc 3"