Login procedure
===============
Created by geyzer on 23/11/15

Start the application

     |USER                     |PASSWORD|
     |-------------------------|--------|
     |gennadiy.kasyan@gmail.com|tutti.ch|


Login with correct both user and password
-----------------------------------------

tags: login

* Go to Settings
* On Settings page select "Account" tab
* On Account tab click "LOGIN" button
* Login with user <USER> and password <PASSWORD>
* On Account tab there is a message "You are logged in as "<USER>