Item removal process
=====================
Remove a specific item
Check it is not in the list anymore

Preconditions:
* Add a new item with description "My item desc 10"

Remove an item
----------------

* Swipe right on item "My item desc 10"
* Click on "delete icon"
* Click on "Yes" button
* Check on Your Values page there is no item "My item desc 10"